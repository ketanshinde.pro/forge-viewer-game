var viewer = null
var root = null

function getBrowserSize() {
    var w, h;

    if (typeof document.documentElement !== 'undefined' && typeof document.documentElement.clientWidth !== 'undefined' && document.documentElement.clientWidth !== 0) {
        w = document.documentElement.clientWidth; //IE
        h = document.documentElement.clientHeight;
    }
    else {
        w = document.body.clientWidth; //IE
        h = document.body.clientHeight;
    }
    return { 'width': w, 'height': h };
}

function getForgeToken() {
    return new Promise(function (resolve, reject) {
        let ppApi = new PublicProject();
        ppApi.Authenticate().then(response => {
            console.log("token response : ", response)
            if (response.ok) {
                response.json().then(json => {
                    // console.log(json);
                    resolve({
                        access_token: json.access_token,
                        expires_in: 1800,
                        token_type: "Bearer"
                    });
                });
            }

        }).catch(function (err) {
            console.log("token error: ",err);
            reject(err);
        });
    });
}

function handleTokenRequested(onAccessToken) {
    // console.log('Token requested by the viewer.');

    if (onAccessToken) {
        getForgeToken().then(function (data) {
            try {
                // console.log("forge token : ", data)
                if (data)
                    onAccessToken(data.access_token, 1800);
            }
            catch (ex) {
                console.log(ex)
            }
        });
    }
}

function onDocumentLoadSuccess(viewerDocument) {
    root = viewerDocument.getRoot()
    var defaultModel = root.getDefaultGeometry();
    viewer.loadDocumentNode(viewerDocument, defaultModel);

    viewer.addEventListener(Autodesk.Viewing.GEOMETRY_LOADED_EVENT, () => {
        console.log("root : ", root)
        // viewer.hide(root.id)
        // viewer.setGhosting(false);

        // console.log("data_json : ", data_json)
        // const drawingUtil = new DrawingUtil();
        // Object.freeze(drawingUtil);
        // drawingUtil.drawWells(data_json, viewer);
    })

    // onDocumnetReadyInitialize()
}

function onDocumentLoadFailure() {
    console.error('Failed fetching Forge manifest');
}

function handleViewerInit() {
    var htmlDiv = document.getElementById('forgeViewer');
    viewer = new Autodesk.Viewing.Private.GuiViewer3D(htmlDiv);
    var startedCode = viewer.start();
    if (startedCode > 0) {
        console.error('Failed to create a Viewer: WebGL not supported.');
        return;
    }
    console.log('Initialization complete, loading a model next...');

    Autodesk.Viewing.Document.load("urn:" + btoa("urn:adsk.objects:os.object:cctechfluidvolumebucket/ml-mesh-seg_3LrZTq.stl"), onDocumentLoadSuccess,
        onDocumentLoadFailure);
}

function rootComponentDidMount() {
    let options = {
        env: 'AutodeskProduction', getAccessToken: handleTokenRequested
    };

    Autodesk.Viewing.Initializer(
        options, handleViewerInit);
}