var SHub_API_Core = {

    //Below functions are written open inside class constructor. They work as private functions and not visible in console.

    _get: function(url, processData, isAsync, useAuth) {
        //var header = (useAuth == true) ? { "Authorization": "Bearer " + SHub.Accounts.getAccessToken() } : undefined;
        console.log("url : ", url)
        return fetch(url, {
            type: "GET",
            contentType: "application/json",
            processData: processData,
            async: isAsync,
            //headers: header,
        });
    },

    _post: function(url, processData, isAsync, useAuth, data, contentType) {
        //var header = (useAuth == true) ? { "Authorization": "Bearer " + SHub.Accounts.getAccessToken() } : undefined;
        var reqObj = {
            type: "POST",
            url: url,
            contentType: (contentType === null || contentType === undefined) ? "application/json" : contentType,
            processData: processData,
            //headers: header,
        }
        if (data) {
            reqObj.data = JSON.stringify(data);
        }
        if (isAsync !== null || isAsync !== undefined)
            reqObj.async = isAsync;
        else
            reqObj.async = true;

        return fetch(reqObj);
    },

    //Below functions are wrappers for private functions. Work like public functions.
    Get: function (url, processData, isAsync, useAuth) { return this._get(url, processData, isAsync, useAuth); },
    Post: function (url, processData, isAsync, useAuth, data, contentType) { return this._post(url, processData, isAsync, useAuth, data, contentType); }
}