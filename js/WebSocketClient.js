let WebSocketClient = {
    socket: undefined,
    submitJobCallback: null,
    getUploadLinkCallback: null,
    onUploadComplete: null,
    onCreateProject: null,
    onQueryProject: null,
    onReceiveUrl: null,
    idleBreaker: undefined,
    init: function (onOpenCallback) {
        WebSocketClient.socket = new WebSocket("wss://4lw4jmkoue.execute-api.us-east-1.amazonaws.com/dev");

        WebSocketClient.socket.onopen = function () {
            // console.log("websocket is connected")
            WebSocketClient.setIdleBreaker();
            if (onOpenCallback) onOpenCallback();
        };

        WebSocketClient.socket.onmessage = function (evt) {
            WebSocketClient.setIdleBreaker();
            // console.log("WebSocketClient.socket.readyState : ", WebSocketClient.socket.readyState);
            let data = JSON.parse(evt.data)
            // console.log(data);
            if (typeof (data.action) !== "undefined") {
                // console.log("evt.data.action : ", data.action)
                switch (data.action) {
                    case "get-upload-link":
                        WebSocketClient.getUploadLinkCallback(data)
                        break;

                    case "submit-job":
                        WebSocketClient.submitJobCallback(data)
                        break;

                    case "upload-from-link":
                        WebSocketClient.onUploadComplete(data)
                        break;

                    case "create-project":
                        WebSocketClient.onCreateProject(data)
                        break;

                    case "query-projects":
                        WebSocketClient.onQueryProject(data)
                        break;

                    case "get-geom-info":
                        WebSocketClient.getGeomInfo(data)
                        break;

                    case "get-signed-url":
                        WebSocketClient.onReceiveUrl(data)
                        break;

                    default:
                        break;
                }

            }
        };

        WebSocketClient.socket.onclose = function () {
            WebSocketClient.init();
            // console.log("WebSocket connection is closed.");
        };
    },
    run: function (sendData, callback) {
        WebSocketClient.setIdleBreaker();
        // console.log(sendData);

        if (typeof (sendData) !== "undefined") {

            switch (sendData.action) {
                case "get-upload-link":
                    WebSocketClient.getUploadLinkCallback = callback
                    break;

                case "submit-job":
                    WebSocketClient.submitJobCallback = callback
                    break;

                case "upload-from-link":
                    WebSocketClient.onUploadComplete = callback
                    break;

                case "create-project":
                    WebSocketClient.onCreateProject = callback
                    break;

                case "query-projects":
                    WebSocketClient.onQueryProject = callback
                    break;

                case "get-geom-info":
                    WebSocketClient.getGeomInfo = callback
                    break;

                case "get-signed-url":
                    WebSocketClient.onReceiveUrl = callback
                    break;

                default:
                    break;
            }

            WebSocketClient.socket.send(JSON.stringify(sendData));
        }
    },
    close: function () {
        // Close the connection, if open.
        if (typeof (WebSocketClient.socket) !== 'undefined' && WebSocketClient.socket.readyState === WebSocket.OPEN) {
            WebSocketClient.socket.close();
        }

        WebSocketClient.socket = undefined
        WebSocketClient.submitJobCallback = null
        WebSocketClient.getUploadLinkCallback = null
        WebSocketClient.onUploadComplete = null
        WebSocketClient.onCreateProject = null
        WebSocketClient.onQueryProject = null
        WebSocketClient.onReceiveUrl = null
    },
    setIdleBreaker: function () {
        WebSocketClient.clearIdleBreaker();
        WebSocketClient.idleBreaker = setTimeout(function () {
            let msg = { "action": "idle-breaker" };
            WebSocketClient.socket.send(JSON.stringify(msg));
        }, 595000);
    },
    clearIdleBreaker: function () {
        clearTimeout(WebSocketClient.idleBreaker);
    },
    counter: 0,
    startTime: undefined
};