class PublicProject {
    Authenticate = () => {
        // return SHub_API_Core.default.Get(SHub_Constants.default.SHAPP_API_URL + "api/v2/app/publicproject/authenticate/undefined", false, true, false);
        console.log("SHub_API_Core: ", SHub_API_Core)
        console.log("SHub_Constants: ", SHub_Constants)
        let forgeApiUrl = 'forge-apis'
        if (window.location.href.includes('localhost')) forgeApiUrl += '-dev'
        return SHub_API_Core.Get(SHub_Constants.CCTECH_API_URL + forgeApiUrl + "/get-token", false, true, false);
    };
}