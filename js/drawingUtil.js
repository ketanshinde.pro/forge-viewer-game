class DrawingUtil {
    constructor() {
        this.THREE = THREE;
    }
}

DrawingUtil.prototype.pipeMesh = function (curvePath, pointsLenght) {
    var geometry = new THREE.TubeGeometry(curvePath, pointsLenght * 2, 0.25);
    var material = new this.THREE.MeshPhongMaterial({ color: 0x44ff44 });
    var mesh = new this.THREE.Mesh(geometry, material);

    return mesh;
}

DrawingUtil.prototype.drawWells = function (data_json, viewer) {
    const data = data_json.dataArray;
    const overlayScene = 'Wells_Overlay';
    viewer.impl.removeOverlay(overlayScene);
    viewer.impl.createOverlayScene(overlayScene);

    for (let i = 0; i < data.length; i++) {
        let api_data = data[i];
        let apiOffset = data_json.apiLoc[i]
        var curvePath = new THREE.CurvePath();
        let points_length = api_data.measured_depth__ft.length;
        for (let j = 1; j < points_length; j++) {
            let point1_x = api_data.x_deviation__ft[j-1];
            let point1_y = api_data.y_deviation__ft[j]-1;
            let point1_z = api_data.z_true_vertical_depth__ft[j-1] * -1;
            point1_x = point1_x != 0 ? point1_x / 800 : point1_x;
            point1_y = point1_y != 0 ? point1_y / 800 : point1_y;
            point1_z = point1_z != 0 ? point1_z / 800 : point1_z;

            let point2_x = api_data.x_deviation__ft[j];
            let point2_y = api_data.y_deviation__ft[j];
            let point2_z = api_data.z_true_vertical_depth__ft[j] * -1;
            point2_x = point2_x != 0 ? point2_x / 800 : point2_x;
            point2_y = point2_y != 0 ? point2_y / 800 : point2_y;
            point2_z = point2_z != 0 ? point2_z / 800 : point2_z;

            let pt1 = new this.THREE.Vector3(point1_x + apiOffset[0],
                point1_y + apiOffset[1], point1_z);

            let pt2 = new this.THREE.Vector3(point2_x + apiOffset[0],
                point2_y + apiOffset[1], point2_z);

            let lineCurve = new this.THREE.LineCurve(pt1, pt2);
            curvePath.add(lineCurve);
        }

        let pipe = this.pipeMesh(curvePath, points_length);
        viewer.impl.addOverlay(overlayScene, pipe);
        viewer.impl.invalidate(false, false, true);
    }
}